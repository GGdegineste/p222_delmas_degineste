package com.example.p222_delmas_degineste;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class BD extends SQLiteOpenHelper {
    //Création de la table UTILISATEUR
    public final String SQL_CREATE = "CREATE TABLE UTILISATEUR (id INTEGER PRIMARY KEY, nom TEXT); ";
    //Suppression de la table UTILISATEURs
    public final String SQL_DELETE = "DROP TABLE UTILISATEUR IF EXISTS UTILISATEUR;";


    public BD(Context context) {
        super(context, "utilisateur.db", null, 1);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(this.SQL_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL(this.SQL_DELETE);
        onCreate(db);
    }
}
