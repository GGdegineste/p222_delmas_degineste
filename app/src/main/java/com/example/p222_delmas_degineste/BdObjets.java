package com.example.p222_delmas_degineste;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class BdObjets extends SQLiteOpenHelper {

    public final String SQL_CREATE = "CREATE TABLE ITEM (id INTEGER PRIMARY KEY, nom TEXT, co2 TEXT); ";
    public final String SQL_DELETE = "DROP TABLE ITEM IF EXISTS ITEMS;";



    public BdObjets(Context context) {
        super(context, context.getString(R.string.nomBDD), null, 1);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(this.SQL_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL(this.SQL_DELETE);
        onCreate(db);
    }
}
