package com.example.p222_delmas_degineste;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface AirPollutionAPI {

    //APPEL de l'API airPollution de OpenWeatherMap entrée: Latitude, Longitude, APPID délivré par mail
    @GET("http://api.openweathermap.org/data/2.5/air_pollution")
    Call<Pojo.IndiceAir> getAQI(@Query("lat")String lat, @Query("lon") String lon, @Query("appid") String appid);
}
