package com.example.p222_delmas_degineste;

import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class CustomAdapter extends ArrayAdapter<String> {

    private ArrayList<String> nomSet;
    private ArrayList<String> co2Set;
    private Context mContext;
    private ArrayList<String> idSet;
    String unite;

    public CustomAdapter(@NonNull Context context, ArrayList<String> idSet, ArrayList<String> nomSet,ArrayList<String> co2Set,String unite) {
        super(context,R.layout.item_list_layout, idSet);
        this.idSet=idSet;
        this.nomSet=nomSet;
        this.co2Set=co2Set;
        this.mContext=context;
        this.unite=unite;
    }

    private static class ViewHolder{
        TextView nom;
        TextView co2;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent){
        String stringId = getItem(position);
        //a partir de l'id on récupere les données adéquates (l'id est la position dans la liste -1)
        int intId = Integer.parseInt(stringId);
        String nom = nomSet.get(intId-1);
        String co2 = co2Set.get(intId-1);
        co2+=" "+unite;
        ViewHolder viewHolder = new ViewHolder();
        if(convertView==null){
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.item_list_layout,parent,false);
            viewHolder.nom = convertView.findViewById(R.id.nomListeItem);
            viewHolder.co2 = convertView.findViewById(R.id.co2ListeItem);
            convertView.setTag(viewHolder);
        }else{
            convertView.getTag();
        }
        if(viewHolder!=null && viewHolder.nom!=null &&  viewHolder.co2!=null){
            viewHolder.nom.setText(nom);
            viewHolder.co2.setText(co2);
        }
        return convertView;
    }

}
