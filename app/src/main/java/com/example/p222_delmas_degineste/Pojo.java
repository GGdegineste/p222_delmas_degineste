package com.example.p222_delmas_degineste;




import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

//Ensemble de classes générées a partir de JSON2POJO pour (JSON de airPollutionAPI)
public class Pojo {


    public class Components {

        @SerializedName("co")
        @Expose
        private Float co;
        @SerializedName("no")
        @Expose
        private Float no;
        @SerializedName("no2")
        @Expose
        private Float no2;
        @SerializedName("o3")
        @Expose
        private Float o3;
        @SerializedName("so2")
        @Expose
        private Float so2;
        @SerializedName("pm2_5")
        @Expose
        private Float pm25;
        @SerializedName("pm10")
        @Expose
        private Float pm10;
        @SerializedName("nh3")
        @Expose
        private Float nh3;

        public Float getCo() {
            return co;
        }

        public void setCo(Float co) {
            this.co = co;
        }

        public Float getNo() {
            return no;
        }

        public void setNo(Float no) {
            this.no = no;
        }

        public Float getNo2() {
            return no2;
        }

        public void setNo2(Float no2) {
            this.no2 = no2;
        }

        public Float getO3() {
            return o3;
        }

        public void setO3(Float o3) {
            this.o3 = o3;
        }

        public Float getSo2() {
            return so2;
        }

        public void setSo2(Float so2) {
            this.so2 = so2;
        }

        public Float getPm25() {
            return pm25;
        }

        public void setPm25(Float pm25) {
            this.pm25 = pm25;
        }

        public Float getPm10() {
            return pm10;
        }

        public void setPm10(Float pm10) {
            this.pm10 = pm10;
        }

        public Float getNh3() {
            return nh3;
        }

        public void setNh3(Float nh3) {
            this.nh3 = nh3;
        }

    }


    public class Coord {

        @SerializedName("lon")
        @Expose
        private Float lon;
        @SerializedName("lat")
        @Expose
        private Float lat;

        public Float getLon() {
            return lon;
        }

        public void setLon(Float lon) {
            this.lon = lon;
        }

        public Float getLat() {
            return lat;
        }

        public void setLat(Float lat) {
            this.lat = lat;
        }

    }


    public class IndiceAir {

        @SerializedName("coord")
        @Expose
        private Coord coord;
        @SerializedName("list")
        @Expose
        private java.util.List<com.example.p222_delmas_degineste.Pojo.List> list = null;

        public Coord getCoord() {
            return coord;
        }

        public void setCoord(Coord coord) {
            this.coord = coord;
        }

        public java.util.List<com.example.p222_delmas_degineste.Pojo.List> getList() {
            return list;
        }

        public void setList(java.util.List<com.example.p222_delmas_degineste.Pojo.List> list) {
            this.list = list;
        }

        @Override
        public String toString() {
            return "latitude: " + this.getCoord().lat + "\n" + "longitude: " + this.getCoord().lon + "\n\n" +
                    "TimeStamp UNIX: " + this.getList().get(0).getDt() + "\n\n" +
                    "Qualité de l'air: " + (6-this.getList().get(0).getMain().getAqi()) + "/5 \n\n" +
                    "Details: \n\n"
                    + "NO: " + this.getList().get(0).getComponents().getNo() + "                μg/m3\n"
                    + "NO2: " + this.getList().get(0).getComponents().getNo2() + "        μg/m3\n"
                    + "O3: " + this.getList().get(0).getComponents().getO3() + "         μg/m3\n"
                    + "SO2: " + this.getList().get(0).getComponents().getSo2() + "         μg/m3\n"
                    + "PM2_5: " + this.getList().get(0).getComponents().getPm25() + "\n"
                    + "PM10: " + this.getList().get(0).getComponents().getPm10() + "\n"
                    + "NH3: " + this.getList().get(0).getComponents().getNh3() + "         μg/m3\n";
        }

    }


    public class List {

        @SerializedName("main")
        @Expose
        private Main main;
        @SerializedName("components")
        @Expose
        private Components components;
        @SerializedName("dt")
        @Expose
        private Integer dt;

        public Main getMain() {
            return main;
        }

        public void setMain(Main main) {
            this.main = main;
        }

        public Components getComponents() {
            return components;
        }

        public void setComponents(Components components) {
            this.components = components;
        }

        public Integer getDt() {
            return dt;
        }

        public void setDt(Integer dt) {
            this.dt = dt;
        }

    }


    public class Main {

        @SerializedName("aqi")
        @Expose
        private Integer aqi;

        public Integer getAqi() {
            return aqi;
        }

        public void setAqi(Integer aqi) {
            this.aqi = aqi;
        }

    }
}
