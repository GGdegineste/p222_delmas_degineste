package com.example.p222_delmas_degineste;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.Display;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.Surface;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    //0 > refuser et 1 > accepter
    private boolean etatRadioBtn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Création et instanciation de la base de donnée utilisateur
        BD bd = new BD(this);
        SQLiteDatabase utilisateur = bd.getWritableDatabase();



        TextView nom = findViewById(R.id.textNom);
        TextView rgpd = findViewById(R.id.textRGPD);
        RadioGroup groupe = findViewById(R.id.idGroupe);
        RadioButton accepter = findViewById(R.id.idBtnOui);
        RadioButton refuser = findViewById(R.id.idBtnNon);
        Button btnValider = findViewById(R.id.btnValider);
        btnValider.setEnabled(false);
        btnValider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (etatRadioBtn) {
                    //Nom à enregistrer dans la BD
                    ContentValues values = new ContentValues();
                    values.put("nom", nom.getText().toString());
                    utilisateur.insert("utilisateur", null, values );

                    Intent intent = new Intent(MainActivity.this, EcranPrincipal.class);
                    startActivity(intent);
                    finish();
                } else {
                    System.exit(0);
                }

            }
        });
           accepter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etatRadioBtn = true;
                btnValider.setEnabled(true);
            }
        });
        refuser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etatRadioBtn = false;
                btnValider.setEnabled(true);
            }
        });


    }

}