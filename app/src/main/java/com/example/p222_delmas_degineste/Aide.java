package com.example.p222_delmas_degineste;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Aide extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aide);

        Button quitter = findViewById(R.id.idQuitterAide);
        quitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentRetour = new Intent();
                intentRetour.putExtra("cle","param");
                setResult(RESULT_OK,intentRetour);
                finish();
            }
        });



    }
}