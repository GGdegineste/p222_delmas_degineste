package com.example.p222_delmas_degineste;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;


public class EcranPrincipal extends AppCompatActivity {

    private static int CODE_RETOUR = 22;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ecran_principal);
        //Création et instanciation de la base de donnée utilisateur
        BD bd = new BD(this);
        SQLiteDatabase utilisateur = bd.getWritableDatabase();

        TextView messageBienvenu = findViewById(R.id.textView3);

        String[] col = {"id", "nom"};
        String[] select = {};
        String nom = "";
        Cursor curs = utilisateur.query("utilisateur", col, "", select, null, null, null);
        if (curs.moveToFirst()) {
            do {
                nom = (curs.getString(curs.getColumnIndexOrThrow("nom")));
            } while (curs.moveToNext());
        }
        curs.close();
        messageBienvenu.setText(getResources().getString(R.string.messageBienvenuFR) + " " + nom);
        //boutons
        Button un = findViewById(R.id.idGuilhaume);
        Button deux = findViewById(R.id.idTheo);
        un.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentGuilhaume = new Intent(EcranPrincipal.this, EcranGuilhaume.class);
                startActivity(intentGuilhaume);
            }
        });
        deux.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentTheo = new Intent(EcranPrincipal.this, EcranTheo.class);
                startActivity(intentTheo);
            }
        });

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_principal, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.idAide:
                Intent intentAide = new Intent(EcranPrincipal.this, Aide.class);
                startActivityForResult(intentAide,CODE_RETOUR);
                return true;
            case R.id.idQuitter:
                System.exit(0);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK && requestCode==CODE_RETOUR){
            Toast.makeText(getApplicationContext(),getResources().getString(R.string.clic_retour_aide),Toast.LENGTH_SHORT).show();
        }else{
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.non_clic_retour_aide), Toast.LENGTH_SHORT).show();
        }

    }
}