package com.example.p222_delmas_degineste;

import android.app.Activity;
import android.app.AppComponentFactory;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import java.io.File;
import java.util.ArrayList;


public class EcranTheo extends AppCompatActivity {

    private static int CODE_RETOUR = 22;

    private TextView mTextView;
    ArrayList<String> listeId;
    ArrayList<String> listeNom;
    ArrayList<String> listeCo2;
    CustomAdapter adapter;
    ListView listeView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ecran_theo);
        mTextView = (TextView) findViewById(R.id.text);
        listeView = (ListView) findViewById(R.id.list_view_item);
        listeView.setAdapter(adapter);

        //si la base de donnée n'existe pas, on la créer et on la remplit
        if(!checkDatabaseExists()){
            Toast.makeText(getApplicationContext(),getResources().getString(R.string.creationBDD),Toast.LENGTH_SHORT).show();
            BdObjets bdd = new BdObjets(this);
            fillDatabase(bdd);
        }

        Button loading_btn = findViewById(R.id.loading_btn);
        loading_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BdObjets bdd = new BdObjets(EcranTheo.this);
                SQLiteDatabase dbw = bdd.getReadableDatabase();
                listeId = new ArrayList<String>();
                listeNom = new ArrayList<String>();
                listeCo2 = new ArrayList<String>();
                adapter = new CustomAdapter(EcranTheo.this,listeId,listeNom,listeCo2,getResources().getString(R.string.uniteKg));
                String[] col = {getResources().getString(R.string.bddId), getResources().getString(R.string.bddNomFr),getResources().getString(R.string.bddCo2)};
                String[] select = {};
                String nom = "";
                Cursor curs = dbw.query(getResources().getString(R.string.bddTable), col, "", select, null, null, null);
                if (curs.moveToFirst()) {
                    do {
                        listeId.add(curs.getString(curs.getColumnIndexOrThrow(getResources().getString(R.string.bddId))));
                        listeNom.add(curs.getString(curs.getColumnIndexOrThrow(getResources().getString(R.string.bddNomFr))));
                        listeCo2.add(curs.getString(curs.getColumnIndexOrThrow(getResources().getString(R.string.bddCo2))));
                    } while (curs.moveToNext());
                }
                curs.close();
                listeView.setAdapter(adapter);
            }
        });
    }

    //retourne true si la base de donnée existe, false sinon
    private boolean checkDatabaseExists(){
        File bdFile = getApplicationContext().getDatabasePath(getResources().getString(R.string.nomBDD));
        return bdFile.exists();
    }

    private void fillDatabase(BdObjets bdd){
        SQLiteDatabase dbw = bdd.getWritableDatabase();

        dbw.execSQL("insert into item values(1,'Televiseur','52,7')");
        dbw.execSQL("insert into item values(2,'Ordi Portable','42,1')");
        dbw.execSQL("insert into item values(3,'Ecran','39,9')");
        dbw.execSQL("insert into item values(4,'Tablette','22,7')");
        dbw.execSQL("insert into item values(5,'Smartphone','18,7')");
        dbw.execSQL("insert into item values(6,'Liseuse','8,3')");
        dbw.execSQL("insert into item values(7,'Console Video','20,4')");
        dbw.execSQL("insert into item values(8,'Imprimante','38,2')");
        dbw.execSQL("insert into item values(9,'Appareil Photo','4,3')");
        dbw.execSQL("insert into item values(10,'Réfrigérateur','35,0')");
        dbw.execSQL("insert into item values(11,'Lave-Linge','42,7')");
        dbw.execSQL("insert into item values(12,'Lave-Vaisselle','47,1')");
        dbw.execSQL("insert into item values(13,'Four electrique','17,3')");
        dbw.execSQL("insert into item values(14,'Gaziniere','44,8')");
        //Source : https://www.lemonde.fr/climat/article/2018/12/13/quel-est-le-bilan-carbone-des-objets-du-quotidien_5397063_1652612.html
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_principal, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.idAide:
                Intent intentAide = new Intent(EcranTheo.this, Aide.class);
                startActivityForResult(intentAide,CODE_RETOUR);
                return true;
            case R.id.idQuitter:
                System.exit(0);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK && requestCode==CODE_RETOUR){
            Toast.makeText(getApplicationContext(),getResources().getString(R.string.clic_retour_aide),Toast.LENGTH_SHORT).show();
        }else{
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.non_clic_retour_aide), Toast.LENGTH_SHORT).show();
        }

    }
}