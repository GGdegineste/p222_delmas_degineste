package com.example.p222_delmas_degineste;


import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.provider.Settings;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class EcranGuilhaume extends AppCompatActivity {

    //LOG
    private static final String idCallLog = "CALL_API";


    Button btnGetLocationAndInfo;
    TextView showInfo;

    //LOCALISATION
    LocationManager locationManager;
    private static final int REQUEST_LOCATION = 1;

    //RETROFIT
    Retrofit retrofit;

    //SERVICE API
    AirPollutionAPI indiceAir;
    @SerializedName("lat")
    String latitude;
    @SerializedName("lon")
    String longitude;
    @SerializedName("appid")
    private static final String APPID = "c739c374d7c49bb63f34218c80f8658f";
    private static final String BASE_URL = "http://api.openweathermap.org/data/2.5/";
    //Attribut résultat de l'API AirPollution
    Pojo.IndiceAir result;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ecran_guilhaume);
        ActivityCompat.requestPermissions( this,
                new String[] {Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
        this.showInfo = findViewById(R.id.showInfo);
        //RETROFIT
        this.retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        this.indiceAir = retrofit.create(AirPollutionAPI.class);

        this.btnGetLocationAndInfo = findViewById(R.id.btnGetLocationAndInfo);
        this.btnGetLocationAndInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    OnGPS();
                } else {
                    getLocation();
                }

                //API CALL
                Call<Pojo.IndiceAir> call = indiceAir.getAQI(latitude, longitude, APPID);
                call.enqueue(new Callback<Pojo.IndiceAir>() {
                    @Override
                    public void onResponse(Call<Pojo.IndiceAir> call, Response<Pojo.IndiceAir> response) {
                        if (response.isSuccessful()) {
                            //On récupère directement avec la classe POJO
                            result =  response.body();
                            showInfo.setText(result.toString());
                            Log.e(idCallLog, result.toString());
                        }
                    }

                    @Override
                    public void onFailure(Call<Pojo.IndiceAir> call, Throwable t) {
                        Log.e(idCallLog, t.getMessage());
                    }
                });

            }


        });

    }

    private void OnGPS() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.demandeGPS).setCancelable(false).setPositiveButton(R.string.btnValider, new  DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
            }
        }).setNegativeButton(R.string.btnRefuser, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void getLocation() {
        if (ActivityCompat.checkSelfPermission(
                EcranGuilhaume.this,Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                EcranGuilhaume.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
        } else {
            Location locationGPS = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if (locationGPS != null) {
                double lat = locationGPS.getLatitude();
                double longi = locationGPS.getLongitude();
                latitude = String.valueOf(lat);
                longitude = String.valueOf(longi);
                Toast.makeText(this,"\n" + "Latitude: " + latitude + "\n" + "Longitude: " + longitude, Toast.LENGTH_SHORT).show();

            } else {
                Toast.makeText(this, R.string.localisationError, Toast.LENGTH_SHORT).show();
            }
        }
    }
}